from unittest import TestCase, mock
from oneservice import Microservice


class MockFlask(object):
    def __init__(self, name=""):
        self.run_called = False

    def run(self, host="0.0.0.0", port="5000"):
        self.run_called = True


def mock_ms_init(self, handler, sv=""):
    self.app = MockFlask()


def empty_handler(data):
    pass


class TestMicroservice(TestCase):
    def test_init_(self):
        Microservice(handler=empty_handler)

    @mock.patch("oneservice.Microservice.__init__", mock_ms_init)
    def test_start(self):
        m = Microservice(handler=empty_handler)
        m.start()

        self.assertTrue(m.app.run_called)

    def test_health_endpoint(self):
        m = Microservice(
            handler=empty_handler, service_version="1.2.3", enable_health_endpoint=True
        )
        test_client = m.app.test_client()

        response = test_client.get("/health")

        self.assertTrue(200, response.status_code)
        self.assertEqual(b"1.2.3", response.data)

    def test_handler_called(self):
        def sample_handler(json_data):
            return {"message": json_data["message"] + " " + "polo"}, 200

        m = Microservice(handler=sample_handler, handler_method="POST")

        test_client = m.app.test_client()
        response = test_client.post("/", json={"message": "marco"})

        self.assertTrue(200, response.status_code)
        self.assertIsNotNone(response.json)
        self.assertIn("message", response.json)
        self.assertEqual("marco polo", response.json["message"])

    def test_validation(self):
        def sample_handler(json_data):
            return {"message": "polo"}, 200

        def sample_validator_failed(json_data):
            return ["Error message"]

        m = Microservice(
            handler=sample_handler,
            handler_method="POST",
            data_validator=sample_validator_failed,
        )

        test_client = m.app.test_client()
        response = test_client.post("/", json={"message": "marco"})

        self.assertTrue(400, response.status_code)
        self.assertIsNotNone(response.json)
        self.assertIn("errors", response.json)
        self.assertEqual("Error message", response.json["errors"][0])

        def sample_validator_passed(json_data):
            return []

        m2 = Microservice(
            handler=sample_handler,
            handler_method="POST",
            data_validator=sample_validator_passed,
        )

        test_client = m2.app.test_client()
        response = test_client.post("/", json={"message": "marco"})

        self.assertTrue(200, response.status_code)
        self.assertIsNotNone(response.json)
        self.assertIn("message", response.json)
        self.assertEqual("polo", response.json["message"])
