"""
    Example 2 for using DDV Microservice

    Remember to export PYTHONPATH=../ before running,
    or install the package locally
"""
from oneservice import Microservice


def add_numbers(json_data: dict) -> (dict, int):
    try:
        return {"result": int(json_data["a"]) + int(json_data["b"])}, 200
    except TypeError:
        return {"error": "'a' and 'b' must be integers"}, 400


def validate_parameters(json_data: dict) -> list:
    errors = []

    required_params = ["a", "b"]

    for rp in required_params:
        if rp not in json_data:
            errors.append(f"'{rp}' is a required parameter")

    return errors


m = Microservice(
    handler=add_numbers,
    service_version="1.0.0",
    data_validator=validate_parameters,
    handler_method="POST",
    enable_health_endpoint=True,
)

m.start()

# curl http://localhost:5000/health
# curl -X POST -H "Content-Type: application/json" --data '{"a": 2, "b": 2}' http://localhost:5000/
# curl -X POST -H "Content-Type: application/json" --data '{"a": 2}' http://localhost:5000/
