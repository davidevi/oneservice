"""
    Example 1 for using DDV Microservice

    Remember to export PYTHONPATH=../ before running,
    or install the package locally
"""
from oneservice import Microservice


def return_doubled(json_data: dict) -> (dict, int):
    return {"result": int(json_data["a"]) * 2}, 200


m = Microservice(handler=return_doubled)

m.start()

# curl http://localhost:5000/health
# curl -X POST -H "Content-Type: application/json" --data '{"a": 2}' http://localhost:5000/
