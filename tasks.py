from invoke import run


def mypy():
    run("poetry run python -m mypy oneservice", pty=True, warn=True)


def flake8():
    run("poetry run python -m flake8 oneservice", pty=True, warn=True)


def check():
    mypy()
    flake8()


def test():
    run(
        "poetry run pytest --cov=oneservice --cov-branch tests", pty=True, warn=True,
    )
